/**
 * @file main.cpp
 *
 * @brief Implementation of main
 *
 * @author   Ítalo Muñoz
 * @date     Jun, 2019
 * @revision $Revision: 1$
 *
 * @copyright 2019 Autentia. All Rights Reserved.
 *
 * @section LICENSE
 *
 * Confidential Information of Autentia. Not for disclosure or distribution
 * prior written consent. This software contains code, techniques and
 * know-how which is confidential and proprietary to Autentia.
 *
 * Use of this software is subject to the terms of an end user license
 * agreement.
 */

/* INCLUDES ******************************************************************/

#include <HelloWorld.h>
#include <iostream>

/* IMPLEMENTATION ************************************************************/

int main(int argc, char** argv)
{
	for (int i = 0; i < argc; ++i)
	{
		std::cout << "argv[" << i << "]=<" << argv[i] << ">" << std::endl;
	}

	HelloWorld helloWorldInstance;
}
