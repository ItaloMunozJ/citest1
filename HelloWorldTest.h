/**
 * @file HelloWorldTest.h
 *
 * @brief Declaration of HelloWorldTest class
 *
 * @author   Ítalo Muñoz
 * @date     Jun, 2019
 * @revision $Revision: 1$
 *
 * @copyright 2019 Autentia. All Rights Reserved.
 *
 * @section LICENSE
 *
 * Confidential Information of Autentia. Not for disclosure or distribution
 * prior written consent. This software contains code, techniques and
 * know-how which is confidential and proprietary to Autentia.
 *
 * Use of this software is subject to the terms of an end user license
 * agreement.
 */

#pragma once

/* INCLUDES ******************************************************************/

#include <cxxtest/TestSuite.h>
#include <HelloWorld.h>

/* CLASS DECLARATION *********************************************************/

/**
 * @brief HelloWorldTest ...
 *
 */
class HelloWorldTest : public CxxTest::TestSuite
{
public:
	void Test1()
	{
		TS_ASSERT_EQUALS(1 + 1, 2);
	}

	void TestSumOnHelloWorld()
	{
		HelloWorld helloWorldInstance;
		TS_TRACE("Testing HelloWorld.Sum()");
		TS_ASSERT(helloWorldInstance.Sum(3, 4) == 7);
	}
};
