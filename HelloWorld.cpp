/**
 * @file HelloWorld.cpp
 *
 * @brief Implementation of HelloWorld class
 *
 * @author   Ítalo Muñoz
 * @date     Jun, 2019
 * @revision $Revision: 1$
 *
 * @copyright 2019 Autentia. All Rights Reserved.
 *
 * @section LICENSE
 *
 * Confidential Information of Autentia. Not for disclosure or distribution
 * prior written consent. This software contains code, techniques and
 * know-how which is confidential and proprietary to Autentia.
 *
 * Use of this software is subject to the terms of an end user license
 * agreement.
 */

/* INCLUDES ******************************************************************/

#include <iostream>
#include <HelloWorld.h>

/* CLASS IMPLEMENTATION ******************************************************/

HelloWorld::HelloWorld()
{
	std::cout << "Hello World! My instance is " << std::hex << this << std::dec << std::endl;
}

HelloWorld::~HelloWorld()
{
	std::cout << "Good bye cruel world! I was " << std::hex << this << std::dec << std::endl;
}

int HelloWorld::Sum(int x, int y)
{
	return x + y;
}
