/**
 * @file HelloWorld.h
 *
 * @brief Declaration of HelloWorld class
 *
 * @author   Ítalo Muñoz
 * @date     Jun, 2019
 * @revision $Revision: 1$
 *
 * @copyright 2019 Autentia. All Rights Reserved.
 *
 * @section LICENSE
 *
 * Confidential Information of Autentia. Not for disclosure or distribution
 * prior written consent. This software contains code, techniques and
 * know-how which is confidential and proprietary to Autentia.
 *
 * Use of this software is subject to the terms of an end user license
 * agreement.
 */

#pragma once

/* CLASS DECLARATION *********************************************************/

/**
 * @brief HelloWorld ...
 *
 */
class HelloWorld
{
public:
	/**
	 * @brief Creates a new instance of HelloWorld
	 *
	 */
	explicit HelloWorld();

	/**
	 * @brief Destroy an instance of HelloWorld
	 *
	 */
	virtual ~HelloWorld();

	/**
	 * @brief
	 *
	 */
	int Sum(int x, int y);
};
